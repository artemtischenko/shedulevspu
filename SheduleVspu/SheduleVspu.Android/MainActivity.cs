﻿using Android.App;
using Android.Content.PM;
using Android.Gms.Ads;
using Android.Views;
using Android.OS;
using SegmentedControl.FormsPlugin.Android;

namespace SheduleVspu.Droid
{
    [Activity(Theme = "@style/MainThemeLoad", Label = "Расписание ВГПУ", Icon = "@drawable/ic_launcher", Name = "ru.SheduleVspu.MainActivity", MainLauncher = true, LaunchMode = LaunchMode.SingleInstance, ScreenOrientation = ScreenOrientation.Portrait, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation, WindowSoftInputMode = SoftInput.AdjustResize)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        protected override void OnCreate(Bundle bundle)
        {
           SetTheme(Resource.Style.MainTheme);

            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate(bundle);
            
            global::Xamarin.Forms.Forms.Init(this, bundle);
            SegmentedControlRenderer.Init();
            MobileAds.Initialize(ApplicationContext, "ca-app-pub-8554306855535180~4453695380");
            LoadApplication(new App());
        }
    }
}

