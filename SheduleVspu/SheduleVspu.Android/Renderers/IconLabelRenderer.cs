using Android.Graphics;
using SheduleVspu.Droid.Renderers;
using SheduleVspu.UI.Controls;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(IconLabel), typeof(MyLabelRenderer))]
namespace SheduleVspu.Droid.Renderers
{

    public class MyLabelRenderer : LabelRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Label> e)
        {
            base.OnElementChanged(e);
            var label = Control;
            var font = Typeface.CreateFromAsset(Forms.Context.Assets, "Fonts/IconFont.ttf");
            label.Typeface = font;
        }
    }
}
