using Android.Gms.Ads;
using Android.Widget;
using SheduleVspu.Droid.Renderers;
using SheduleVspu.UI.Controls;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
[assembly: ExportRenderer(typeof(AdsView), typeof(AdsRenderer))]

namespace SheduleVspu.Droid.Renderers
{
    public class AdsRenderer:ViewRenderer<AdsView, AdView>
    {
        private string adUnitId = "ca-app-pub-8554306855535180/1950998581";
        readonly AdSize _adSize = AdSize.Banner;
        private AdView _adView;

        void CreateAddView()
        {
            if (_adView != null) return;
            var adParams = new LinearLayout.LayoutParams(LayoutParams.WrapContent, LayoutParams.WrapContent);
            _adView = new AdView(Forms.Context)
            {
                AdSize = _adSize,
                AdUnitId = adUnitId,
                LayoutParameters = adParams
            };
            _adView.LoadAd(new AdRequest.Builder().Build());
        }

        protected override void OnElementChanged(ElementChangedEventArgs<AdsView> e)
        {
            base.OnElementChanged(e);
            if (Control == null)
            {
                CreateAddView();
                SetNativeControl(_adView);
            }
        }
    }
}