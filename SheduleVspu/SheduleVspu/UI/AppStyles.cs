﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
namespace SheduleVspu.UI
{
    public class AppStyles
    {
        private static readonly Lazy<AppStyles> LazyInstance = new Lazy<AppStyles>(() => new AppStyles(), true);
        private static AppStyles Instance => LazyInstance.Value;

        private Style _defaultLabelStyle;

        public static Style DefaultLabelStyle => Instance._defaultLabelStyle ?? (Instance._defaultLabelStyle =
          GetStyle(typeof(Label), new List<Setter>
          {
                GetSetter(View.VerticalOptionsProperty, LayoutOptions.FillAndExpand),
                GetSetter(View.HorizontalOptionsProperty, LayoutOptions.FillAndExpand),
                GetSetter(Label.HorizontalTextAlignmentProperty, TextAlignment.Center),
                GetSetter(Label.VerticalTextAlignmentProperty, TextAlignment.Center),
          }));

        public static Style GetStyle(Type targetType, IEnumerable<Setter> setters)
        {
            var style = new Style(targetType);

            foreach (var setter in setters)
            {
                style.Setters.Add(setter);
            }
            return style;
        }

        public static Setter GetSetter(BindableProperty property, object value)
        {
            var setter = new Setter
            {
                Property = property,
                Value = value
            };
            return setter;
        }

        public static Color StringToColor(string colorHex)
        {
            return String.IsNullOrEmpty(colorHex) ? Color.Transparent : Color.FromHex(colorHex);
        }
    }

}
