using SheduleVspu.Helpers;
using SheduleVspu.UI.Converters;
using Xamarin.Forms;

namespace SheduleVspu.UI.ViewCells
{
    public class SubjectViewCell : ViewCell
    {
        public SubjectViewCell()
        {
            InitializeUi();
        }

        private void InitializeUi()
        {
            View = new Grid
                {
                    ColumnDefinitions = GridUiExtensions.GenerateColumnDefinition(50, GridLength.Star, 50),
                    RowSpacing = 2,
                    Padding = 2
                }
                .Add
                (
                    new Label
                        {
                            Style = AppStyles.DefaultLabelStyle
                        }
                        .AddBinding(Label.TextProperty, "Time")
                        .SetGridRow(0)
                        .SetGridColumn(0),
                    new StackLayout
                        {
                            VerticalOptions = LayoutOptions.FillAndExpand,
                            HorizontalOptions = LayoutOptions.FillAndExpand,
                            Spacing = 2
                        }
                        .Add(
                            new Label
                                {
                                    HorizontalTextAlignment = TextAlignment.Start,
                                    Style = AppStyles.DefaultLabelStyle,
                                }
                                .AddBinding(Label.TextProperty, "Discipline"),
                            new Label
                                {
                                    HorizontalTextAlignment = TextAlignment.Start,
                                    Style = AppStyles.DefaultLabelStyle
                                }
                                .AddBinding(Label.TextProperty, "Lecturer")
                                .AddBinding(VisualElement.IsVisibleProperty, "IsVisibleLecturer")
                                .SetGridRow(1)
                                .SetGridColumn(1)
                        )
                        .SetGridRow(0)
                        .SetGridColumn(1),
                    new Label
                        {
                            Style = AppStyles.DefaultLabelStyle
                        }
                        .AddBinding(Label.TextProperty, "Auditory")
                        .SetGridRow(0)
                        .SetGridColumn(2)
                );
        }
    }
}