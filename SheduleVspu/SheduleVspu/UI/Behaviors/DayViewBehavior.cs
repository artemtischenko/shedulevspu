using SheduleVspu.UI.Views;
using Xamarin.Forms;

namespace SheduleVspu.UI.Behaviors
{
    public class DayViewBehavior : Behavior<DayView>
    {
        protected override void OnAttachedTo(DayView bindable)
        {
            base.OnAttachedTo(bindable);
            if (bindable.ListView != null) bindable.ListView.ItemSelected +=ListViewOnItemSelected;
        }

        private void ListViewOnItemSelected(object sender, SelectedItemChangedEventArgs selectedItemChangedEventArgs)
        {
            var lv = sender as ListView;
            if (lv != null) lv.SelectedItem = null;
        }

        protected override void OnDetachingFrom(DayView bindable)
        {
            if (bindable.ListView != null) bindable.ListView.ItemSelected-= ListViewOnItemSelected;
            base.OnDetachingFrom(bindable);
        }
    }
}