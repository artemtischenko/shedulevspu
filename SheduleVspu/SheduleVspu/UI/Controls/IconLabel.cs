using Xamarin.Forms;

namespace SheduleVspu.UI.Controls
{
    public class IconLabel : Label
    {
        public IconLabel()
        {
            if (Device.RuntimePlatform == Device.iOS)
            {
                FontFamily = "Eleganticons";
            }
        }
    }
}