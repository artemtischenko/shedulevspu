﻿//  Based on https://github.com/XLabs/Xamarin-Forms-Labs
//  Special thanks to XLabs from Binwell Ltd.

//  Distributed under Apache 2.0 Licence: http://www.apache.org/licenses/LICENSE-2.0

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Windows.Input;
using Xamarin.Forms;

namespace SheduleVspu.UI.Controls
{
    public class RepeaterView : StackLayout, IDisposable
    {
        public static readonly BindableProperty ItemTemplateProperty = BindableProperty.Create(nameof(ItemTemplate), typeof(DataTemplate), typeof(RepeaterView), default(DataTemplate));
        public static readonly BindableProperty ItemsSourceProperty = BindableProperty.Create(nameof(ItemsSource), typeof(IEnumerable), typeof(RepeaterView), null, BindingMode.OneWay, null, ItemsChanged);
        public static readonly BindableProperty ItemClickCommandProperty = BindableProperty.Create(nameof(ItemClickCommand), typeof(ICommand), typeof(RepeaterView), null, BindingMode.TwoWay, null, ItemClickPropertyChanged);

        private static void ItemClickPropertyChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var itemClickCommand = (ICommand)newValue;
            var control = bindable as RepeaterView;
            if (control == null)
                throw new Exception("Invalid bindable object passed to ReapterView::ItemsChanged expected a ReapterView received a " + bindable.GetType().Name);

            foreach (var tapGestureRecognizer in control.TapGestureRecognizers)
                tapGestureRecognizer.Command = itemClickCommand;
        }

        public delegate void RepeaterViewItemAddedEventHandler(object sender, RepeaterViewItemAddedEventArgs args);

        public event RepeaterViewItemAddedEventHandler ItemCreated;

        private IDisposable _collectionChangedHandle;

        private List<TapGestureRecognizer> TapGestureRecognizers { get; } = new List<TapGestureRecognizer>();

        public RepeaterView()
        {
            Spacing = 0;
        }



        public IEnumerable ItemsSource
        {
            get { return (IEnumerable)GetValue(ItemsSourceProperty); }
            set { SetValue(ItemsSourceProperty, value); }
        }

        public ICommand ItemClickCommand
        {
            get { return (ICommand)GetValue(ItemClickCommandProperty); }
            set { SetValue(ItemClickCommandProperty, value); }
        }

        public DataTemplate ItemTemplate
        {
            get { return (DataTemplate)GetValue(ItemTemplateProperty); }
            set { SetValue(ItemTemplateProperty, value); }
        }

        protected virtual void NotifyItemAdded(View view, object model)
        {
            ItemCreated?.Invoke(this, new RepeaterViewItemAddedEventArgs(view, model));
        }

        protected virtual View ViewFor(object item)
        {
            var template = ItemTemplate;
            var content = template?.CreateContent();

            if (!(content is View) && !(content is ViewCell))
                throw new Exception($"Invalid object type: {content?.GetType().Name}");

            var view = (content is View) ? content as View : ((ViewCell)content).View;
            view.BindingContext = item;
            view.AutomationId = item?.ToString() ?? "ListItem";
            var tapGestureRecognizer = new TapGestureRecognizer { Command = ItemClickCommand, CommandParameter = item };
            TapGestureRecognizers.Add(tapGestureRecognizer);
            view.GestureRecognizers.Add(tapGestureRecognizer);
            ForceLayout();
            return view;
        }

        private static void ItemsChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var control = bindable as RepeaterView;
            if (control == null)
                throw new Exception(
                    "Invalid bindable object passed to ReapterView::ItemsChanged expected a ReapterView received a "
                    + bindable.GetType().Name);

            control._collectionChangedHandle?.Dispose();

            control._collectionChangedHandle = new CollectionChangedHandle<View>(
                control.Children,
                newValue as IEnumerable<object>,
                control.ViewFor,
                (v, m, i) => control.NotifyItemAdded(v, m));
        }

        public void Dispose()
        {
            _collectionChangedHandle?.Dispose();
        }

        internal class CollectionChangedHandle<TSyncType> : IDisposable where TSyncType : class
        {
            private readonly Func<object, TSyncType> _projector;
            private readonly Action<TSyncType, object, int> _postadd;
            private readonly Action<TSyncType> _cleanup;
            private readonly INotifyCollectionChanged _itemsSourceCollectionChangedImplementation;
            private readonly IEnumerable<object> _sourceCollection;
            private readonly IList<TSyncType> _target;

            public CollectionChangedHandle(IList<TSyncType> target, IEnumerable<object> source, Func<object, TSyncType> projector, Action<TSyncType, object, int> postadd = null, Action<TSyncType> cleanup = null)
            {
                if (source == null) return;
                _itemsSourceCollectionChangedImplementation = source as INotifyCollectionChanged;
                _sourceCollection = source;
                _target = target;
                _projector = projector;
                _postadd = postadd;
                _cleanup = cleanup;
                InitialPopulation();
                if (_itemsSourceCollectionChangedImplementation == null) return;
                _itemsSourceCollectionChangedImplementation.CollectionChanged += CollectionChanged;
            }

            public void Dispose()
            {
                if (_itemsSourceCollectionChangedImplementation == null) return;
                _itemsSourceCollectionChangedImplementation.CollectionChanged -= CollectionChanged;
            }

            private void CollectionChanged(object sender, NotifyCollectionChangedEventArgs args)
            {
                if (args.Action == NotifyCollectionChangedAction.Reset) SafeClearTarget();
                else
                {
                    var tlist = new List<object>(_sourceCollection);

                    if (args.OldItems != null)
                    {
                        var syncitem = _target[args.OldStartingIndex];
                        if (syncitem != null) _cleanup?.Invoke(syncitem);
                        _target.RemoveAt(args.OldStartingIndex);
                    }

                    if (args.NewItems == null) return;
                    foreach (var obj in args.NewItems)
                    {
                        var item = obj;
                        if (item == null) continue;
                        var index = tlist.IndexOf(item);
                        var newsyncitem = _projector(item);
                        _target.Insert(index, newsyncitem);
                        _postadd?.Invoke(newsyncitem, item, index);
                    }
                }

            }

            private void InitialPopulation()
            {
                SafeClearTarget();
                foreach (var t in _sourceCollection.Where(x => x != null)) _target.Add(_projector(t));
            }

            private void SafeClearTarget()
            {
                while (_target.Count > 0)
                {
                    var syncitem = _target[0];
                    _target.RemoveAt(0);
                    _cleanup?.Invoke(syncitem);
                }
            }
        }

    }


    public class RepeaterViewItemAddedEventArgs : EventArgs
    {
        public RepeaterViewItemAddedEventArgs(View view, object model)
        {
            View = view;
            Model = model;
        }

        public View View { get; set; }
        public object Model { get; set; }
    }
}