﻿using System;
using System.Collections.Generic;
using System.Linq;
using PropertyChanged;
using SegmentedControl.FormsPlugin.Abstractions;
using Xamarin.Forms;

namespace SheduleVspu.UI.Controls
{
  
    [AddINotifyPropertyChangedInterface]
    public class ExtendedSegmentedControl:SegmentedControl.FormsPlugin.Abstractions.SegmentedControl
    {
        public static BindableProperty ChildrenProperty = BindableProperty.Create(nameof(Children), typeof(List<string>), typeof(ExtendedSegmentedControl) , new List<string>(), propertyChanged: PropertyChanged);

       
        public static BindableProperty SelectedItemProperty = BindableProperty.Create(nameof(SelectedItem), typeof(string), typeof(ExtendedSegmentedControl) , default(string),BindingMode.OneWayToSource);
 
        private string SelectedItem
        {
            get { return (string) GetValue(SelectedItemProperty); }
            set { SetValue(SelectedItemProperty,value); }
        }

        public ExtendedSegmentedControl()
        {
            ValueChanged += ValueChangedExecute;
        }

        private void ValueChangedExecute(object sender, EventArgs eventArgs)
        {
            var view = sender as ExtendedSegmentedControl;
            if(view?.Children == null || !view.Children.Any()) return;
            view.SelectedItem = view.Children[view.SelectedSegment]?.Text;
        }

        private new static void PropertyChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var newChildren = newValue as List<string>;

            var view = bindable as SegmentedControl.FormsPlugin.Abstractions.SegmentedControl;
            if (view != null&& newChildren!=null) view.Children = newChildren?.Select(item=>new SegmentedControlOption{Text = item}).ToList()?? new List<SegmentedControlOption>{new SegmentedControlOption{Text = ""}};
        }

        
    }
}
