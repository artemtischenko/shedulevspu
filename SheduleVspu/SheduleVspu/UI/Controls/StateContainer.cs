﻿//  Based on https://github.com/xDelivered-Patrick/Xamarin.Forms.Essentials/blob/master/Essentials/Controls/State/
//  Special thanks to Patrick McCurley from Binwell Ltd.

//  Distributed under Apache 2.0 Licence: http://www.apache.org/licenses/LICENSE-2.0

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using SheduleVspu.Helpers;
using Xamarin.Forms;
using Xamarin.Forms.Internals;

namespace SheduleVspu.UI.Controls
{
    [ContentProperty("Conditions")]
    [Preserve(AllMembers = true)]
    public class StateContainer : Layout
    {
        public List<StateCondition> Conditions { get; set; } = new List<StateCondition>();

        public static readonly BindableProperty IsAnimateStateChangeProperty =
            BindableProperty.Create(nameof(IsAnimateStateChange), typeof(bool), typeof(StateContainer), true,
                BindingMode.Default);

        public bool IsAnimateStateChange
        {
            get { return (bool) GetValue(IsAnimateStateChangeProperty); }
            set { SetValue(IsAnimateStateChangeProperty, value); }
        }

        public static readonly BindableProperty StateProperty = BindableProperty.Create(nameof(State), typeof(object),
            typeof(StateContainer), null, propertyChanged: StateChanged);

        public object State
        {
            get { return GetValue(StateProperty); }
            set { SetValue(StateProperty, value); }
        }

        public StateContainer AddStateBinding(string bindingPath = "State")
        {
            this.AddBinding(StateProperty, bindingPath);
            return this;
        }

        public bool ContainState(object state)
        {
            return Conditions.Any(c => c.State?.Equals(state) ?? false);
        }

        public StateCondition GetState(object state)
        {
            state = state?.ToString();
            return Conditions.FirstOrDefault(c => c.State != null && c.State.ToString().Equals(state));
        }

        private readonly ObservableCollection<Element> _internalChildren;
        private bool _errorState;
        private TaskCompletionSource<bool> _hideCompletionSource;
        private TaskCompletionSource<bool> _showCompletionSource;

        private View Content { get; set; }

        public StateContainer()
        {
            _internalChildren = (this as ILayoutController).Children as ObservableCollection<Element>;
        }

        public StateContainer(string bindingPath = null) : this()
        {
            if (!string.IsNullOrEmpty(bindingPath))
                this.SetBinding(StateProperty, bindingPath);
        }


        private static void StateChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var parent = bindable as StateContainer;
            if (parent == null) return;
            parent._errorState = false;
            parent.ChooseStateProperty(newValue);
        }


        public StateCondition InitState(object newState)
        {
            var condition = GetState(newState);
            if (condition == null)
            {
                return null;
            }
            if (condition.IsInitialized) return condition;
            if (!condition.Init()) return condition;
            return condition;
        }

        public void SetState(object newState)
        {
            var condition = InitState(newState);
            if (condition.Content != null) Device.BeginInvokeOnMainThread(async () => { await ShowState(condition); });
        }

        private async void ChooseStateProperty(object newValue)
        {
            if (_errorState || newValue == null || Conditions == null || Conditions.Count == 0) return;

            try
            {
                Debug.WriteLine($"ChooseStateProperty {newValue}");
                if (_showCompletionSource != null) await _showCompletionSource.Task;
                if (_hideCompletionSource != null) await _hideCompletionSource.Task;

                await HideCurrentContent();
                await ShowState();
            }
            catch
            {
                // ignored System.InvalidOperationException: Collection was modified; enumeration operation may not execute
            }
        }

        public async Task HideCurrentContent()
        {
            // Logger.Message("Start waiting hide");
            if (Content == null) return;
            if (_showCompletionSource != null) await _showCompletionSource.Task;
            if (_hideCompletionSource != null) await _hideCompletionSource.Task;
            if (Content != null && State != null && Content.ClassId == State.ToString()) return;
            _hideCompletionSource = new TaskCompletionSource<bool>();
            var currState = Content.ClassId;
            try
            {
                // Logger.Message("Start hidding " + Content.ClassId);
                if (IsAnimateStateChange)
                {
                    await Content.FadeTo(0, 150U);
                    Content.IsVisible = false;
                }
                await Task.Delay(10); // allow UI thread to clear the view
                try
                {
                    _internalChildren?.Remove(Content);
                }
                catch
                {
                    // ignored
                }
                Debug.WriteLine($"Hidden state  {currState}");
                // Logger.Message("End hidding");
            }
            catch (Exception e)
            {
                Debug.WriteLine($"StateContainer ChooseStateProperty remove old content error: {e}");
            }
            if (!_hideCompletionSource.Task.IsCompleted) _hideCompletionSource.SetResult(true);
        }

        public async Task ShowState(StateCondition stateCondition = null)
        {
            // Logger.Message("Start waiting show");
            if (_showCompletionSource != null) await _showCompletionSource.Task;
            if (_hideCompletionSource != null) await _hideCompletionSource.Task;
            if (stateCondition == null) stateCondition = InitState(State);
            if (stateCondition == null || !stateCondition.IsInitialized) return;
            if (Content != null && Content.ClassId == stateCondition.State.ToString() && Content.IsVisible) return;
            _showCompletionSource = new TaskCompletionSource<bool>();
            try
            {
                Content = stateCondition.Content;

                stateCondition.Content.ClassId = stateCondition.State.ToString();

                if (_internalChildren != null && !_internalChildren.Contains(Content))
                    _internalChildren.Add(Content);
                if (IsAnimateStateChange)
                {
                    stateCondition.Content.IsVisible = false;
                    stateCondition.Content.Opacity = 0;
                }
                InvalidateLayout();

                if (IsAnimateStateChange)
                {
                    Content.IsVisible = true;
                    await Content.FadeTo(1, 150U);
                }
                //  Debug.WriteLine("End show");
                Debug.WriteLine($"Showed state  {stateCondition.State}");
            }
            catch (Exception e)
            {
                Debug.WriteLine($"StateContainer ChooseStateProperty set new content {stateCondition.State} error: {e}");
                Content = GetErrorState();
                _errorState = true;
                if (_internalChildren != null)
                {
                    _internalChildren.Clear();
                    _internalChildren.Add(Content);
                }
                InvalidateLayout();
            }
            if (!_showCompletionSource.Task.IsCompleted) _showCompletionSource.SetResult(true);
        }


        protected override SizeRequest OnMeasure(double widthConstraint, double heightConstraint)
        {
            var rect = CalculateSize(Content, 0, 0, widthConstraint, heightConstraint);
            return !rect.IsEmpty
                ? new SizeRequest(new Size(rect.Width, rect.Height))
                : base.OnMeasure(widthConstraint, heightConstraint);
        }

        protected override void LayoutChildren(double x, double y, double width, double height)
        {
            var content = Content;
            if (content == null) return;
            var rect = CalculateSize(content, x, y, width, height);
            // Debug.WriteLine($@"LayoutChildren {State} {Content} {x}x{y} {width}x{height} in {rect.Width}x{rect.Height}");
            if (!rect.IsEmpty) LayoutChildIntoBoundingRegion(Content, rect);
        }

        private Rectangle CalculateSize(View content, double x, double y, double width, double height)
        {
            if (content == null) return Rectangle.Zero;
            //  var scroll = content as ScrollView;
            //if (scroll?.Content != null) content = scroll.Content;
            var horizontal = content.HorizontalOptions;
            var vertical = content.VerticalOptions;
            var w = content.WidthRequest;
            var h = content.HeightRequest;
            var hr = height;
            var scroll = ParentIsScroll(content);
            if (scroll) hr = double.PositiveInfinity;
            if (w <= 0 & h <= 0)
            {
                var sizeRequest = content.Measure(width, hr, MeasureFlags.IncludeMargins);
                if (w <= 0) w = sizeRequest.Request.Width;
                if (h <= 0) h = sizeRequest.Request.Height;
                if (w < content.MinimumWidthRequest) w = content.MinimumWidthRequest;
                if (h < content.MinimumHeightRequest) h = content.MinimumHeightRequest;
            }

            if (horizontal.Alignment == LayoutAlignment.Center)
            {
                x = Math.Max(x, Math.Min(width, width / 2d - w / 2d));
            }
            if (horizontal.Alignment == LayoutAlignment.End)
            {
                x = Math.Max(x, Math.Min(width, width - w));
            }
            if (horizontal.Alignment == LayoutAlignment.Fill)
            {
                w = width;
            }


            if (vertical.Alignment == LayoutAlignment.Center)
            {
                y = Math.Max(y, Math.Min(height, height / 2d - h / 2d));
            }
            if (vertical.Alignment == LayoutAlignment.End)
            {
                y = Math.Max(y, Math.Min(height, height - h));
            }
            if (vertical.Alignment == LayoutAlignment.Fill)
            {
                if (h < height && !double.IsPositiveInfinity(height)) h = height;
                if (!scroll && h > height) h = height;
            }
            if (w > width) w = width;
            return new Rectangle(x, y, w, h);
        }

        private bool ParentIsScroll(View content)
        {
            if (content == null) return false;
            var parent = content.Parent;
            while (parent != null)
            {
                if (parent is ScrollView) return true;
                parent = parent.Parent;
            }
            return false;
        }

        public View GetErrorState()
        {
            return new StackLayout
            {
                HorizontalOptions = LayoutOptions.FillAndExpand,
                VerticalOptions = LayoutOptions.FillAndExpand,
                BackgroundColor = Color.White
            }.Add(
                new Label
                {
                    Text = @"¯\_(ツ)_/¯",
                    FontSize = 60,
                    FontAttributes = FontAttributes.Bold,
                    TextColor = Color.Orange,
                    HorizontalOptions = LayoutOptions.Center,
                    Margin = new Thickness(10, 60, 10, 60)
                },
                new Label
                {
                    TextColor = Color.Black,
                    FontSize = 20,
                    HorizontalTextAlignment = TextAlignment.Center,
                    HorizontalOptions = LayoutOptions.Center,
                    Margin = 10,
                    Text =
                        "Простите нас =(\n\nМы не смогли открыть эту страницу...\nНо попробуйте снова, вдруг получится."
                });
        }




    }



    [ContentProperty("Content")]
    public class StateCondition : View
    {

        public enum StateAnimations
        {
            Fade,
            None,
            ToLeft,
            ToRight
        }

        public StateCondition()
        {
        }

        protected internal StateCondition(object state)
        {
            State = state;
        }

        public bool IsInitialized => Content != null;

        public bool Init()
        {
            if (Content != null) return true;
            if (ContentFunc == null) return false;
            Content = ContentFunc.Invoke();
            return true;
        }

        public StateAnimations AnimationIn { get; set; } = StateAnimations.Fade;
        public StateAnimations AnimationOut { get; set; } = StateAnimations.Fade;
        public int AnimationInDuration { get; set; } = 150;
        public int AnimationOutDuration { get; set; } = 150;
        public object State { get; set; }
        public View Content { get; set; }
        public Func<View> ContentFunc { get; set; }

        public override string ToString()
        {
            return $@"Condition for {State} / {Content}";
        }
    }
}
