﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace SheduleVspu.UI.Views.States
{
    public class LoadingView : ContentView
    {
        public LoadingView()
        {
            Content = new ContentView
            {
                BackgroundColor = Color.White,
                VerticalOptions = LayoutOptions.FillAndExpand,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                Content = new ActivityIndicator
                {
                    HeightRequest = 50,
                    WidthRequest = 50,
                    VerticalOptions = LayoutOptions.Center,
                    HorizontalOptions = LayoutOptions.Center,
                    IsRunning = true,
                    Color = Consts.MainBlueColor
                }
            };
        }
    }
}
