﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SheduleVspu.Helpers;
using SheduleVspu.UI.Controls;
using Xamarin.Forms;

namespace SheduleVspu.UI.Views.States
{
    public class NoDataView : ContentView
    {
        public NoDataView()
        {
            Content = new StackLayout
                {
                    Padding = 15,
                    HorizontalOptions = LayoutOptions.Center,
                    VerticalOptions = LayoutOptions.Center,
                    Spacing = 10
                }
                .Add
                (
                    new IconLabel
                    {
                        FontSize = 80,
                        HorizontalOptions = LayoutOptions.Center,
                        Text = "q",
                        TextColor = Consts.AccentRedColor
                    },
                    new Label
                    {
                        FontAttributes = FontAttributes.Bold,
                        FontSize = 16,
                        HorizontalOptions = LayoutOptions.Center,
                        HorizontalTextAlignment = TextAlignment.Center,
                        Text = "Данные не найдены",
                        TextColor = Color.FromHex("#777777")
                    }

                );
        }
    }
}
