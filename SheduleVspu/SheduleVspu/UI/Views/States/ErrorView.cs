﻿using SheduleVspu.Helpers;
using SheduleVspu.UI.Controls;
using Xamarin.Forms;

namespace SheduleVspu.UI.Views.States
{
    public class ErrorView: ContentView
    {
        public ErrorView()
        {
            Content = new Grid
                {
                    RowDefinitions = GridUiExtensions.GenerateRowDefinition(GridLength.Star, GridLength.Auto)
                }
                .Add
                (
                    new StackLayout
                        {
                            Padding = 15,
                            HorizontalOptions = LayoutOptions.Center,
                            VerticalOptions = LayoutOptions.Center,
                            Spacing = 10
                        }
                        .Add
                        (
                            new IconLabel()
                            {
                                FontSize = 80,
                                HorizontalOptions = LayoutOptions.Center,
                                Text = "q",
                                TextColor = Consts.AccentRedColor
                            },
                            new Label
                            {
                                FontAttributes = FontAttributes.Bold,
                                FontSize = 16,
                                HorizontalOptions = LayoutOptions.Center,
                                HorizontalTextAlignment = TextAlignment.Center,
                                Text = "Возникла ошибка",
                                TextColor = Color.FromHex("#777777")
                            },
                            new Label
                            {
                                HorizontalOptions = LayoutOptions.Center,
                                HorizontalTextAlignment = TextAlignment.Center,
                                Text = "Пожалуйста, проверьте соединение\nи повторите попытку"
                            }
                        ).SetGridRow(0),
                        new Button
                        {
                            Margin = 10,
                            BackgroundColor = Consts.AccentGreenColor,
                            HeightRequest = 55,
                            Text = "Обновить",
                            TextColor = Color.White
                        }
                        .SetGridRow(1)
                        .AddBinding(Button.CommandProperty, "RepeatCommand")
                        
                );
        }
    }
}
