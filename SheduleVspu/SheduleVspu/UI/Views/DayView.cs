using SheduleVspu.Helpers;
using SheduleVspu.UI.Behaviors;
using SheduleVspu.UI.ViewCells;
using Xamarin.Forms;

namespace SheduleVspu.UI.Views
{
    public class DayView : ContentView
    {
        public ListView ListView;

        public DayView()
        {
            InitializeUi();
            Behaviors.Add(new DayViewBehavior());
        }

        private void InitializeUi()
        {
            Content = new StackLayout()
                .Add(
                    new Label
                        {
                            HeightRequest = 30,
                            FontAttributes = FontAttributes.Bold,
                            FontSize = 15,
                            TextColor = Consts.MainBlueColor,
                            Style = AppStyles.DefaultLabelStyle
                        }
                        .AddBinding(Label.TextProperty, "Name"),
                    new ListView
                        {
                            ItemTemplate = new DataTemplate(typeof(SubjectViewCell)),
                            HasUnevenRows = true
                        }
                    .ToVariable(out ListView)
                    .AddBinding(ListView.ItemsSourceProperty, "Subjects")
                   
                        
                );
        }
    }
}