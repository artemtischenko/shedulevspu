﻿using Xamarin.Forms;

namespace SheduleVspu.UI
{
    public static class Consts
    {
        public static readonly Color MainBlueColor = Color.FromHex("#34459d");
        public static readonly Color AccentRedColor = Color.FromHex("#9d3446");
        public static readonly Color AccentGreenColor = Color.FromHex("#469d34");
    }
}
