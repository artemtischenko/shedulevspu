﻿using SheduleVspu.Helpers;
using SheduleVspu.UI.Animations;
using SheduleVspu.UI.Controls;
using SheduleVspu.UI.Views.States;
using Xamarin.Forms;

namespace SheduleVspu.UI.Pages
{
    public class SettingsPage : BasePage
    {
        public static BindableProperty StateProperty =BindableProperty.Create(nameof(State), typeof(object), typeof(BasePage), propertyChanged: StatePropertyChanged);

        private static void StatePropertyChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var page = bindable as SettingsPage;
            page?._storyboard.Go(page.State);
        }

        public object State
        {
            get { return GetValue(StateProperty); }
            set { SetValue(StateProperty, value);}
        }

       
        readonly Storyboard _storyboard = new Storyboard();
        View _activityIndicator;
        View _completedButton;
        Layout<View> _profileStack;
        Layout<View> _courseStack;
        Layout<View> _subGroupStack;

        public SettingsPage()
        {
            Title = "Настройки";

            if (Application.Current?.MainPage?.Navigation?.NavigationStack?.Count >= 1)
            {
                var toolbarItem = new ToolbarItem
                    {
                        Text = "Закрыть",
                        Icon = "close.png"
                    }
                    .AddBinding(MenuItem.CommandProperty, "NavigateBackCommand");
                ToolbarItems.Add(toolbarItem);
            }
            UnitializeUi();
            CreateAnimations();

            this.AddBinding(StateProperty, "AnimationState");
        }

        View GetNormalView()
        {
            return  new AbsoluteLayout()
                {
                    VerticalOptions = LayoutOptions.FillAndExpand,
                    HorizontalOptions = LayoutOptions.FillAndExpand,
                    Padding = 10
                }
                .Add
                (
                    new ActivityIndicator
                        {
                            BackgroundColor = Color.AliceBlue,
                            HeightRequest = 50,
                            VerticalOptions = LayoutOptions.Center,
                            HorizontalOptions = LayoutOptions.Center,
                            WidthRequest = 50,
                            Color = Consts.MainBlueColor,
                            IsRunning = true
                        }
                        .SetAbsoluteLayoutParams(new Rectangle(0.5, 0.5, 50, 50), AbsoluteLayoutFlags.All)
                        .ToVariable(out _activityIndicator),
                    new StackLayout
                    {
                        IsVisible = false,
                        Opacity = 0,
                    }
                        .Add(
                            new Label {Text = "Профиль"},
                            new Picker()
                             
                                .AddBinding(Picker.SelectedItemProperty, "ProfileSelectedItem")
                                .AddBinding(Picker.ItemsSourceProperty, "ProfileItemsSource")
                                )
                                .ToVariable(out _profileStack)
                                .SetAbsoluteLayoutParams(new Rectangle(0, 0, 1, 50),
                            AbsoluteLayoutFlags.PositionProportional | AbsoluteLayoutFlags.WidthProportional),
                    new StackLayout
                    {
                        IsVisible = false,
                        Opacity = 0,
                    }
                        .Add(
                            new Label {Text = "Курс"},
                            new Picker()
                                .AddBinding(Picker.SelectedItemProperty, "CourseSelectedItem")
                                .AddBinding(Picker.ItemsSourceProperty, "CourceItemsSource")
                        )
                        .ToVariable(out _courseStack)
                        .SetAbsoluteLayoutParams(new Rectangle(0, 70, 1, 50),
                            AbsoluteLayoutFlags.WidthProportional | AbsoluteLayoutFlags.XProportional),
                    new StackLayout
                    {
                        IsVisible = false,
                        Opacity = 0,
                    }
                        .Add(
                            new Label {Text = "Подгруппа"},
                            new Picker()
                                .AddBinding(Picker.SelectedItemProperty, "SubgroupSelectedItem")
                                .AddBinding(Picker.ItemsSourceProperty, "SubgroupItemsSource")
                        )
                        .ToVariable(out _subGroupStack)
                        .SetAbsoluteLayoutParams(new Rectangle(0, 150, 1, 50),
                            AbsoluteLayoutFlags.XProportional | AbsoluteLayoutFlags.WidthProportional),
                    new Button
                        {
                            BackgroundColor = Consts.AccentGreenColor,
                            IsVisible = false,
                            Opacity = 0,
                            Text = "Готово",
                            VerticalOptions = LayoutOptions.FillAndExpand,
                            HorizontalOptions = LayoutOptions.FillAndExpand,
                            TextColor = Color.White
                        }
                        .SetAbsoluteLayoutParams(new Rectangle(0, 1, 1, 50),
                            AbsoluteLayoutFlags.PositionProportional | AbsoluteLayoutFlags.WidthProportional)
                        .AddBinding(Button.CommandProperty, "CompleteCommand")
                        .ToVariable(out _completedButton)
                );
         }

        private void CreateAnimations()
        {
            _storyboard.Add(AnimationState.Start, new[]
            {
                new ViewTransition(_activityIndicator, AnimationType.Opacity, 1, 300), 
            });
            _storyboard.Add(AnimationState.CompletedLoadingPofile, new[]
            {
                new ViewTransition(_activityIndicator, AnimationType.Opacity, 0, 300),
                new ViewTransition(_profileStack, AnimationType.Opacity, 1, 300), 
            }
            );
            _storyboard.Add(AnimationState.LoadingOther, new[]
                {
                    new ViewTransition(_activityIndicator, AnimationType.Opacity, 1, 300), 
                }
            );
            _storyboard.Add(AnimationState.LoadedOther, new[]
                {
                    new ViewTransition(_activityIndicator, AnimationType.Opacity, 0, 300), 
                    new ViewTransition(_courseStack, AnimationType.Opacity, 1, 300),
                    new ViewTransition(_subGroupStack, AnimationType.Opacity, 1, 300), 
                }
            );
            _storyboard.Add(AnimationState.Completed, new[]
                {
                    new ViewTransition(_completedButton, AnimationType.Opacity, 1, 300), 
                    new ViewTransition(_activityIndicator, AnimationType.Opacity, 0, 300),

                }
            );
            _storyboard.Add(AnimationState.LoadingSubjects, new[]
                {
                    new ViewTransition(_activityIndicator, AnimationType.Opacity, 1, 300),
                    new ViewTransition(_completedButton, AnimationType.Opacity, 0, 300),
                    new ViewTransition(_profileStack, AnimationType.Opacity, 0, 300),
                    new ViewTransition(_courseStack, AnimationType.Opacity, 0, 300),
                    new ViewTransition(_subGroupStack, AnimationType.Opacity, 0, 300),


                }
            );
        }

        void UnitializeUi()
        {
            var stateContainer = new StateContainer("PageState");
            stateContainer.Conditions.AddRange
            (
                new[]
                {
                    new StateCondition {State = PageStates.Normal, Content = GetNormalView()},
                    new StateCondition {State = PageStates.Error, Content = new ErrorView()},
                }
            );
            Content = stateContainer;
        }

       

    
    }
}