using System;
using SheduleVspu.Helpers;
using SheduleVspu.UI.Controls;
using SheduleVspu.UI.Views;
using SheduleVspu.UI.Views.States;
using Xamarin.Forms;


namespace SheduleVspu.UI.Pages
{
    public class ShedulePage : BasePage
    {
        public ShedulePage()
        {
            var toolbarItem = new ToolbarItem
            {
                Text = "Настройки",
                Icon = "settings.png"
            }
            .AddBinding(MenuItem.CommandProperty, "NavigateToSettingCommand");

            ToolbarItems.Add(toolbarItem);
            Title = "Расписание";
            InitialiezeUi();

        }





        private void InitialiezeUi()
        {
            var stateContainer = new StateContainer("PageState");
            stateContainer.Conditions.AddRange(
                new[]
                {
                    new StateCondition {State = PageStates.Normal, Content = GetNormalState()},
                    new StateCondition {State = PageStates.Error, Content = new ErrorView()},
                    new StateCondition {State = PageStates.Loading, Content = new LoadingView()},
                    new StateCondition {State = PageStates.NoData, Content = new NoDataView()},
                }
            );
            var grid = new Grid
                {
                    RowDefinitions = GridUiExtensions.GenerateRowDefinition(GridLength.Star, GridLength.Auto)
                }
                .Add(
                    stateContainer
                        .SetGridRow(0),
                    new AdsView()
                        .SetGridRow(1)
                );
            Content = grid;
        }





        private static View GetNormalState()
        {
            return new StackLayout
            {
                Padding = 10,
                VerticalOptions = LayoutOptions.FillAndExpand,
                Spacing = 12,
            }
                .Add(
                  new ExtendedSegmentedControl
                  {
                      TintColor = Consts.MainBlueColor,
                      SelectedTextColor = Color.White,
                  }
                  .AddBinding(ExtendedSegmentedControl.SelectedItemProperty, "SelectedItem")
                  .AddBinding(ExtendedSegmentedControl.ChildrenProperty, "NumeratorItemSource"),
                new CarouselView
                {
                    ItemTemplate = new DataTemplate(typeof(DayView))
                }
                .AddBinding(ItemsView.ItemsSourceProperty, "Numerator.Days")
                );
        }
    }

   
}