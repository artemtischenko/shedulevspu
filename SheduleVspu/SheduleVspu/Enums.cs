﻿namespace SheduleVspu
{
    public enum Pages
    {
        Settings,
        Shedule
    }
    public enum NavigationType
    {
        Normal,
        Modal,
        Custom
    }

    public enum PageStates
    {
        Loading,
        Normal,
        Error,
        NoData
    }

    public enum AnimationState
    {
        Start,
        CompletedLoadingPofile,
        CompletedLoading,
        LoadingOther,
        LoadedOther,
        Completed,
        LoadingSubjects
    }

}
