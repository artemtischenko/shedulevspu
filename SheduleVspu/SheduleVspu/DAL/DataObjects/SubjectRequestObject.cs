﻿namespace SheduleVspu.DAL.DataObjects
{
    public class SubjectRequestObject
    {
        public string Profile { get; set; }
        public string SubGroup { get; set; }
        public string Cource { get; set; }
    }
}