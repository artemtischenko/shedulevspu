﻿namespace SheduleVspu.DAL.DataObjects
{
    public class SubjectResponseObject
    {
        public string AudNum { get; set; }
        public string Time { get; set; }
        public string DayWeek { get; set; }
        public string Numerator { get; set; }
        public string Lecturer { get; set; }
        public string Discipline { get; set; }

    }
}
