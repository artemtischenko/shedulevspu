namespace SheduleVspu.DAL.DataService
{
    public struct RequestResult
    {
        public readonly string Message;
        public readonly RequestStatus Status;

        public RequestResult(RequestStatus status, string message = null)
        {
            Status = status;
            Message = message;
            System.Diagnostics.Debug.WriteLine(@"RequestResult: " + message);
            //var list = Newtonsoft.Json.JsonConvert.DeserializeObject<List<string>>(reader.Read().ToString());
        }

        public override string ToString() { return $"Result: {Status}"; }


    }
    public struct RequestResult<T>
    {
        public readonly string Message;
        public readonly RequestStatus Status;
        public readonly T Data;

        public RequestResult(T data, RequestStatus status, string message = null)
        {
            Data = data;
            Status = status;
            Message = message;
        }

        public override string ToString() { return $"Result: {Status} Data: {Data}"; }
    }

    public enum RequestStatus
    {
        Unknown = 0,
        Ok = 200,
        NotModified = 304,
        BadRequest = 400,
        Unauthorized = 401,
        Forbidden = 403,
        NotFound = 404,
        InternalServerError = 500,
        ServiceUnavailable = 503,
        Canceled = 1001,
        InvalidRequest = 1002,
        SerializationError = 1003,
        AlreadyHasOrder = 422,
    }
}
