using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace SheduleVspu.DAL.DataService.—ourseDataService
{
    public interface ICourseDataService
    {
        Task<RequestResult<List<string>>> GetAllAsync(string profile, CancellationToken cts);

    }
}