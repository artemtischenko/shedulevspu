﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace SheduleVspu.DAL.DataService.СourseDataService
{
    public class СourseDataService : BaseRemoteDataService<IApiService>, ICourseDataService
    {
        public СourseDataService() : base(Consts.ApiBaseAddress)
        {
        }

        public async Task<RequestResult<List<string>>> GetAllAsync(string profile, CancellationToken cts)
        {
            var result = await MakeRequest(token => ApiService.GetCourses(profile), cts);
            return new RequestResult<List<string>>(result.Data == null ? null : result.Data, result.Status,
                result.Message);
        }
    }
}
