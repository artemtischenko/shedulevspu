﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace SheduleVspu.DAL.DataService.ProfileDataService
{
    public interface IProfileDataService
    {
        Task<RequestResult<List<string>>> GetAllAsync(CancellationToken cts);
    }
}
