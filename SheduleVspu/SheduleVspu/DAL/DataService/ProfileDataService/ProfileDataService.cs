﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace SheduleVspu.DAL.DataService.ProfileDataService
{
    public class ProfileDataService : BaseRemoteDataService<IApiService>, IProfileDataService
    {
        public ProfileDataService() : base(Consts.ApiBaseAddress)
        {
        }

        public async Task<RequestResult<List<string>>> GetAllAsync(CancellationToken cts)
        {
            var result = await MakeRequest(token => ApiService.GetProfiles(), cts);
            return new RequestResult<List<string>>(result.Data == null ? null : result.Data, result.Status, result.Message);
        }
    }
}
