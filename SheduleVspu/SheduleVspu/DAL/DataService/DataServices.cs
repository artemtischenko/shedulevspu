﻿using SheduleVspu.DAL.DataService.ProfileDataService;
using SheduleVspu.DAL.DataService.SubGroupDataService;
using SheduleVspu.DAL.DataService.SubjectDataService;
using SheduleVspu.DAL.DataService.СourseDataService;

namespace SheduleVspu.DAL.DataService
{
    public static class DataServices
    {
        private static bool _isInitialized;
        public static void Init()
        {
            if(_isInitialized) return;
            _isInitialized = true;
            Profile = new ProfileDataService.ProfileDataService();
            Subgroup = new SubGroupDataService.SubGroupDataService();
            Course = new СourseDataService.СourseDataService();
            Subject = new SubjectDataService.SubjectDataService();
        }

        public static IProfileDataService Profile { get; private set; }
        public static ISubgroupDataService Subgroup{ get; private set; }
        public static ICourseDataService Course { get; private set; }
        public static ISubjectDataService Subject{ get; private set; }
    }
}
