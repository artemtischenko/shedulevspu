﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Polly;
using Refit;

namespace SheduleVspu.DAL.DataService
{
    public class BaseRemoteDataService<TApi>
    {
        private readonly HttpClient _client;
        private readonly int _requestTimeoutSeconds;
        public int RetryCount { get; set; } = 10;

        public int RetryDelayMilliseconds { get; set; } = 300;

        public TApi ApiService { get; private set; }

        protected string TokenString => @"Token token=";

        protected string AuthorizationHeader { get; set; }

        public void SetAuthorizationHeader(string authorizationHeader)
        {
            AuthorizationHeader = authorizationHeader;
        }

        public BaseRemoteDataService(string apiBaseAddress, int requestTimeoutSeconds = 10)
        {
            _requestTimeoutSeconds = requestTimeoutSeconds;

            _client = new HttpClient()
            {
                BaseAddress = new Uri(apiBaseAddress),
                Timeout = TimeSpan.FromSeconds(requestTimeoutSeconds)
            };
            ApiService = RestService.For<TApi>(_client);
        }

        protected async Task<RequestResult<T>> MakeRequest<T>(Func<CancellationToken, Task<T>> loadingFunction,
            CancellationToken cancellationToken, int timeout = -1)
        {
            Exception exception = null;
            var result = default(T);

            _client.Timeout = TimeSpan.FromSeconds(timeout > 0 ? timeout : _requestTimeoutSeconds);

            try
            {
                result = await Policy.Handle<WebException>().Or<HttpRequestException>()
                    .RetryAsync(RetryCount, (ex, span) => exception = ex)
                    .ExecuteAsync(loadingFunction, cancellationToken);
            }
            catch (OperationCanceledException e)
            {
                exception = e;

            }
            catch (Exception e)
            {
                exception = e;
            }

            if (timeout > 0) _client.Timeout = TimeSpan.FromSeconds(timeout);
            var refitException = exception as ApiException;
            return new RequestResult<T>(result, StatusFromException(exception),
                refitException?.Content ?? exception?.Message);
        }

        protected async Task<RequestResult> MakeRequest(Func<CancellationToken, Task> loadingFunction,
            CancellationToken cancellationToken)
        {
            Exception exception = null;
            _client.Timeout = TimeSpan.FromSeconds(_requestTimeoutSeconds);
            try
            {
                await Policy.Handle<WebException>().Or<HttpRequestException>()
                    //                    .WaitAndRetryAsync(RetryCount, i => TimeSpan.FromMilliseconds(RetryDelayMilliseconds), (ex, span) => exception = ex)
                    .RetryAsync(RetryCount, (ex, span) => exception = ex)
                    .ExecuteAsync(loadingFunction, cancellationToken);
            }
            catch (Exception e)
            {
                exception = e;
            }

            var refitException = exception as ApiException;
            return new RequestResult(StatusFromException(exception), refitException?.Content ?? exception?.Message);
        }


        private static RequestStatus StatusFromException(Exception exception)
        {
            if (exception == null) return RequestStatus.Ok;

            var canceledException = exception as TaskCanceledException;
            if (canceledException != null) return RequestStatus.Canceled;

            var operationCanceledException = exception as OperationCanceledException;
            if (operationCanceledException != null) return RequestStatus.Canceled;

            var jsonException = exception as JsonException;
            if (jsonException != null) return RequestStatus.SerializationError;

            var webException = exception as WebException;

            var response = webException?.Response as HttpWebResponse;
            if (response != null) return StatusFromCode((int) response.StatusCode);

            webException = (exception as HttpRequestException)?.InnerException as WebException;

            var apiException = exception as ApiException;
            if (apiException != null)
            {
                return StatusFromCode((int) apiException.StatusCode);
            }

            response = webException?.Response as HttpWebResponse;
            return response != null ? StatusFromCode((int) response.StatusCode) : RequestStatus.Unknown;
        }

        private static RequestStatus StatusFromCode(int code)
        {
            RequestStatus status;
            if (!Enum.TryParse(code.ToString(), out status))
                status = RequestStatus.Unknown;
            return status;
        }
    }
}

     


