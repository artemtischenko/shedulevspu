﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Threading.Tasks;
using Refit;
using SheduleVspu.DAL.DataObjects;

namespace SheduleVspu.DAL.DataService
{
    [SuppressMessage("ReSharper", "LocalizableElement")]
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    [Headers("Accept: application/json")]
    public interface IApiService
    {
        [Get("/Profile")]
        Task<List<string>> GetProfiles();

        [Post("/Сourse?profile={profile}")]
        Task<List<string>> GetCourses(string profile);

        [Post("/SubGroup?profile={profile}")]
        Task<List<string>> GetSubGroups(string profile);

        [Post("/Subject")]
        Task<List<SubjectResponseObject>> GetSubjects([Body] SubjectRequestObject requestObject);


    }
}
