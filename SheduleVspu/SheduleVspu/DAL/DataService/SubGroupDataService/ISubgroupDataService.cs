﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace SheduleVspu.DAL.DataService.SubGroupDataService
{
    public interface ISubgroupDataService
    {
        Task<RequestResult<List<string>>> GetAllAsync(string profile, CancellationToken cts);
    }
}
