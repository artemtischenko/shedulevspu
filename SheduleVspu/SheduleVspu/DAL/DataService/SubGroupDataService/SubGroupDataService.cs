﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace SheduleVspu.DAL.DataService.SubGroupDataService
{
    public class SubGroupDataService : BaseRemoteDataService<IApiService>, ISubgroupDataService
    {
        public SubGroupDataService() : base(Consts.ApiBaseAddress)
        {
        }
        public async Task<RequestResult<List<string>>> GetAllAsync(string profile, CancellationToken cts)
        {
            var result = await MakeRequest(token => ApiService.GetSubGroups(profile), cts);
            return new RequestResult<List<string>>(result.Data == null ? null : result.Data, result.Status, result.Message);
        }
    }
}