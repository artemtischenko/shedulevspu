﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using SheduleVspu.DAL.DataObjects;

namespace SheduleVspu.DAL.DataService.SubjectDataService
{
    public interface ISubjectDataService
    {
        Task<RequestResult<List<SubjectResponseObject>>> GetAllAsync(SubjectRequestObject requestObject, CancellationToken cts);
    }
}
