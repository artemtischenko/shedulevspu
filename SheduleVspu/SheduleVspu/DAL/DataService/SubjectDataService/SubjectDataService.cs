﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using SheduleVspu.DAL.DataObjects;

namespace SheduleVspu.DAL.DataService.SubjectDataService
{
    public class SubjectDataService : BaseRemoteDataService<IApiService>, ISubjectDataService
    {
        public SubjectDataService() : base(Consts.ApiBaseAddress)
        {
        }
    
        public async Task<RequestResult<List<SubjectResponseObject>>> GetAllAsync(SubjectRequestObject requestObject, CancellationToken cts)
        {
            var result = await MakeRequest(token => ApiService.GetSubjects(requestObject), cts);
            return new RequestResult<List<SubjectResponseObject>>(result.Data == null ? null : result.Data, result.Status, result.Message);
        }
    }
}