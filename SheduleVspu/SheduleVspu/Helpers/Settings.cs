// Helpers/Settings.cs

using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Plugin.Settings;
using Plugin.Settings.Abstractions;
using SheduleVspu.DAL.DataObjects;

namespace SheduleVspu.Helpers
{
	/// <summary>
	/// This is the Settings static class that can be used in your Core solution or in any
	/// of your client applications. All settings are laid out the same exact way with getters
	/// and setters. 
	/// </summary>
	public static class Settings
	{
		private static ISettings AppSettings
		{
			get
			{
				return CrossSettings.Current;
			}
		}

		#region Setting Constants

		private const string SubGroupKey = "SubGroup";
		private const string ProfileKey = "Profile";
		private const string CourseKey = "Course";
		private const string SubjectsKey = "Subjects";
		private static readonly string SettingsDefault = string.Empty;

		#endregion


		public static string SubGroup
        {
			get
			{
				return AppSettings.GetValueOrDefault(SubGroupKey, string.Empty);
			}
			set
			{
				AppSettings.AddOrUpdateValue(SubGroupKey, value);
			}
		}
        public static string Profile
        {
			get
			{
				return AppSettings.GetValueOrDefault(ProfileKey, string.Empty);
			}
			set
			{
				AppSettings.AddOrUpdateValue(ProfileKey, value);
			}
		}
        public static string Coursey
        {
			get
			{
				return AppSettings.GetValueOrDefault(CourseKey, string.Empty);
			}
			set
			{
				AppSettings.AddOrUpdateValue(CourseKey, value);
			}
		}

	    public static List<SubjectResponseObject> Subjects
	    {
            get
            {
                var savedData = AppSettings.GetValueOrDefault(SubjectsKey, null);
                if (string.IsNullOrWhiteSpace(savedData)) return null;
                var value = JsonConvert.DeserializeObject<List<SubjectResponseObject>>(savedData);
                return value;
            }
	        set
	        {
	            if (value == null) return;
	            var serializedString = JsonConvert.SerializeObject(value);
	            AppSettings.AddOrUpdateValue(SubjectsKey, serializedString);
	        }
	    }
	}
}