﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;

namespace SheduleVspu.Helpers
{
    internal class BlockedCommand: ICommand
    {
        private readonly Action<object> _executeObject;
        private readonly Action _execute;

        public BlockedCommand(Action<object> executeObject)
        {
            _executeObject = executeObject;
        }

        public BlockedCommand(Action execute)
        {
            _execute = execute;
        }


        public BlockedCommand(Action<object> executeObject, Func<object, bool> canExecute) 
        {
        }

        public BlockedCommand(Action executeObject, Func<bool> canExecute) 
        {
        }

        public bool CanExecute(object parameter)
        {
            return !_isBusy;
        }

     

        public event EventHandler CanExecuteChanged;
      

        int _delay = 700;
        bool _isBusy;
        public async void Execute(object parameter)
        {
            if(_isBusy) return;
            _isBusy = true;
            _execute?.Invoke();
            await Task.Delay(TimeSpan.FromMilliseconds(_delay));
            _isBusy = false;
        }
    }
}
