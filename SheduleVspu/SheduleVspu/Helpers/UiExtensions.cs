﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace SheduleVspu.Helpers
{
    public static class UiExtensions
    {
        public static View SetGridRow(this View view, int rowNumber)
        {
            Grid.SetRow(view, rowNumber);
            return view;
        }

        public static View SetAbsoluteLayoutParams(this View view, Rectangle rectangle, AbsoluteLayoutFlags absoluteLayoutFlags)
        {
            AbsoluteLayout.SetLayoutBounds(view, rectangle);
            AbsoluteLayout.SetLayoutFlags(view, absoluteLayoutFlags);
            return view;
        }
        public static View SetGridColumn(this View view, int columnNumber)
        {
            Grid.SetColumn(view, columnNumber);
            return view;
        }
        public static View SetRowSpan(this View view, int rowSpan)
        {
            Grid.SetRowSpan(view, rowSpan);
            return view;
        }

        public static T ToVariable<T>(this T view, out T outView)
        {
            outView = view;
            return view;
        }
        public static Layout<View> Add(this Layout<View> layout, params View[] views)
        {
            foreach (var view in views)
            {
                layout.Children.Add(view);
            }
            return layout;
        }

        public static T AddBinding<T>(this T obj, BindableProperty bindingProperty, string path, IValueConverter converter = default(IValueConverter))
        {
            var bindable = obj as BindableObject;
            bindable?.SetBinding(bindingProperty, new Binding{Path = path, Converter = converter});
            return obj;
        }
        public static BindableObject AddBinding(this BindableObject bindableObject, BindableProperty bindingProperty, string path, IValueConverter converter = default(IValueConverter))
        {
            bindableObject.SetBinding(bindingProperty, new Binding{Path = path, Converter = converter});
            return bindableObject;
        }
        public static ToolbarItem AddBinding(this ToolbarItem toolbarItem, BindableProperty bindingProperty, string path, IValueConverter converter = default(IValueConverter))
        {
            toolbarItem.SetBinding(bindingProperty, new Binding{Path = path, Converter = converter});
            return toolbarItem;
        }

        public static View SetBindingContext(this View view, object o)
        {
            view.BindingContext = o;
            return view;
        }
    }

    public static class GridUiExtensions
    {
        public static RowDefinitionCollection GenerateRowDefinition(params GridLength[] gridLengths)
        {
            var rowDefinitionCollection  = new RowDefinitionCollection();
            foreach (var l in gridLengths)
            {
                rowDefinitionCollection.Add(new RowDefinition {Height = l});
            }
            return rowDefinitionCollection;
        }

        public static ColumnDefinitionCollection GenerateColumnDefinition(params GridLength[] gridLengths)
        {
            var rowDefinitionCollection = new ColumnDefinitionCollection();
            foreach (var l in gridLengths)
            {
                rowDefinitionCollection.Add(new ColumnDefinition() { Width = l });
            }
            return rowDefinitionCollection;
        }
    }
}
