﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Input;
using SheduleVspu.DAL.DataObjects;
using SheduleVspu.DAL.DataService;
using SheduleVspu.Helpers;
using SheduleVspu.UI.Pages;
using Xamarin.Forms;

namespace SheduleVspu.BL.ViewModels
{
    public class SettingsViewModel : BaseViewModel
    {
        public SettingsViewModel()
        {
            PageState = PageStates.Normal;
        }
        private ICommand _completeCommand;
        private ICommand _navigateBackCommand;
        private ICommand _repeatCommand;
        private string _profileSelectedItem;
        private string _courseSelectedItem;
        private string _subgroupSelectedItem;

        public ICommand CompleteCommand => _completeCommand ?? (_completeCommand = new BlockedCommand(CompleteCommandExecute));
        public ICommand NavigateBackCommand => _navigateBackCommand ?? (_navigateBackCommand = new BlockedCommand(NavigateBackCommandExecute));
        public ICommand RepeatCommand => _repeatCommand?? (_repeatCommand= new Command(RepeatCommandExecute));

        private async void RepeatCommandExecute()
        {
            await LoadData();
        }

        private void NavigateBackCommandExecute()
        {
            NavigateBack(NavigationType.Modal);
        }

        public List<string> ProfileItemsSource { get; set; }
        public List<string> CourceItemsSource { get; set; }
        public List<string> SubgroupItemsSource { get; set; }
        public string ProfileSelectedItem
        {
            get { return _profileSelectedItem; }
            set
            {
                _profileSelectedItem = value;
                ProfileSelectedItemChanged(value);
            }
        }

        public string CourseSelectedItem
        {
            get { return _courseSelectedItem; }
            set
            {
                _courseSelectedItem = value;
                CheckCompleted();
            }
        }

        public string SubgroupSelectedItem
        {
            get { return _subgroupSelectedItem; }
            set
            {
                _subgroupSelectedItem = value;
                CheckCompleted();
            }
        }

        public void CheckCompleted()
        {
                if (!string.IsNullOrWhiteSpace(CourseSelectedItem) && 
                    !string.IsNullOrWhiteSpace(SubgroupSelectedItem))
                {
                    AnimationState = AnimationState.Completed;
                }
        }
        private async void CompleteCommandExecute()
        {
            AnimationState = AnimationState.LoadingSubjects;
            var requestObject = new SubjectRequestObject
            {
                Profile = ProfileSelectedItem,
                Cource = CourseSelectedItem,
                SubGroup = SubgroupSelectedItem
            };
            var response = await DataServices.Subject.GetAllAsync(requestObject, CancellationToken);
            if (response.Status == RequestStatus.Ok && response.Data != null)
            {
                Settings.Subjects = response.Data;
            }
            NavigateTo(Pages.Shedule, NavigationType.Custom);
        }



        
        private async void ProfileSelectedItemChanged(string value)
        {
            AnimationState = AnimationState.LoadingOther;
            var taskCource = DataServices.Course.GetAllAsync(value, CancellationToken);
            var taskSubgroup = DataServices.Subgroup.GetAllAsync(value, CancellationToken);
            await Task.WhenAll(taskCource, taskSubgroup);
            var courceResponse = taskCource.Result;
            var subgroupResponse = taskSubgroup.Result;
            if (courceResponse.Status == RequestStatus.Ok && courceResponse.Data != null &&
                subgroupResponse.Status == RequestStatus.Ok && subgroupResponse.Data != null)
            {
                CourceItemsSource = courceResponse.Data;
                SubgroupItemsSource= subgroupResponse.Data;
                AnimationState = AnimationState.LoadedOther;
                return;
            }
            PageState = PageStates.Error;
        }

        private async Task LoadData()
        {
            PageState = PageStates.Normal;
            AnimationState = AnimationState.Start;
            var response = await DataServices.Profile.GetAllAsync(CancellationToken);
            if (response.Status == RequestStatus.Ok && response.Data != null)
            {
                ProfileItemsSource = response.Data;
                AnimationState = AnimationState.CompletedLoadingPofile;
                PageState = PageStates.Normal;
                return;
            }
            PageState = PageStates.Error;
        }

        public AnimationState AnimationState { get; set; }

        public override async Task InitAsync()
        {
            if (IsInitialized) return;
            await base.InitAsync();
            await LoadData();
        }

    }
}
