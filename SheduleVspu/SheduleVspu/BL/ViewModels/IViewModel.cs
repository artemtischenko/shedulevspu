﻿using System.Threading.Tasks;
using Xamarin.Forms;

namespace SheduleVspu.BL.ViewModels
{
    public interface IViewModel
    {
        Task InitAsync();
        Task FinalizeAsync();
        INavigation Navigation { get; set; }
    }
}