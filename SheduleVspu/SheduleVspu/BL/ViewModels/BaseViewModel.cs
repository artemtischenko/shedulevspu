using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using PropertyChanged;
using Xamarin.Forms;

namespace SheduleVspu.BL.ViewModels
{
    [AddINotifyPropertyChangedInterface]
    public class BaseViewModel : IViewModel, IDisposable
    {
#pragma warning disable 1998
        public virtual async Task InitAsync()
#pragma warning restore 1998
        {
            IsInitialized = true;
        }

        public PageStates PageState { get; set; } = PageStates.Loading;
        public async void NavigateTo(Pages pageName, NavigationType navigationType = NavigationType.Normal)
        {
            try
            {
                var pageTypes = GetAssemblyPageTypes();
                var pageType = pageTypes[pageName.ToString()];
                var page = (Page) Activator.CreateInstance(pageType);
                switch (navigationType)
                {
                    case NavigationType.Normal:
                        await Navigation.PushAsync(page);
                        break;
                    case NavigationType.Modal:
                        await Navigation.PushModalAsync(new NavigationPage(page));
                        break;
                    case NavigationType.Custom:
                        var mainPage = Application.Current.MainPage;
                        var navigationPage = mainPage as NavigationPage;
                        if (navigationPage != null)
                        {
                            mainPage.Navigation.InsertPageBefore(page, navigationPage.CurrentPage);
                            mainPage.Navigation.RemovePage(navigationPage.CurrentPage);
                        }
                        await Navigation.PopModalAsync(true);
                        break;
                    default:
                        throw new ArgumentOutOfRangeException(nameof(navigationType), navigationType, null);
                }
            }
            catch (Exception e)
            {
                 
            }

        }

        public void NavigateBack(NavigationType navigationType = NavigationType.Normal)
        {
            switch (navigationType)
            {
                case NavigationType.Normal:
                    Navigation.PopAsync(true);
                    break;
                case NavigationType.Modal:
                    Navigation.PopModalAsync(true);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(navigationType), navigationType, null);
            }
        }
        static string GetBaseName(TypeInfo typeInfo) { return typeInfo.Name.Replace("Page", "").Replace("ViewModel", ""); }

        static IDictionary<string, Type> GetAssemblyPageTypes()
        {
            return typeof(BasePage).GetTypeInfo().Assembly.DefinedTypes
                .Where(ti => ti.IsClass && !ti.IsAbstract && ti.Name.Contains("Page") && ti.BaseType.Name.Contains("Page"))
                .ToDictionary(GetBaseName, ti => ti.AsType());
        }
        private readonly CancellationTokenSource _cancellationTokenSource = new CancellationTokenSource();
        public CancellationToken CancellationToken => _cancellationTokenSource.Token;
        public bool IsInitialized { get; private set; }
        public async Task FinalizeAsync()
        {
             Dispose();
        }

        public INavigation Navigation { get; set; }

        public void Dispose()
        {
            _cancellationTokenSource?.Dispose();
        }
    }
}