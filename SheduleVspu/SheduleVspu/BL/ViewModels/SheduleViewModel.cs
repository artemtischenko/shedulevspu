﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using SheduleVspu.BL.Models;
using SheduleVspu.Helpers;
using Xamarin.Forms;

namespace SheduleVspu.BL.ViewModels
{
    public class SheduleViewModel : BaseViewModel
    {
        public List<NumeratorModel> Numerators { get; set; }
        public List<string> NumeratorItemSource => Numerators?.Select(item => item.Name).ToList();
        private ICommand _navigateToSettingCommand;

        public ICommand NavigateToSettingCommand
            => _navigateToSettingCommand ?? (_navigateToSettingCommand = new BlockedCommand(NavigateToSettingCommandExecute));

        private void NavigateToSettingCommandExecute()
        {
            NavigateTo(Pages.Settings, NavigationType.Modal);
        }

        public NumeratorModel Numerator { get; set; }

        public string SelectedItem
        {
            set { SelectedNumeratorChanged(value); }
        }

        private void SelectedNumeratorChanged(string value)
        {
            Numerator = Numerators?.FirstOrDefault(numerator => numerator.Name == value);
        }

        public override async Task InitAsync()
        {
            if (IsInitialized) return;
            await base.InitAsync();
            if (!Settings.Subjects.Any())
            {
                PageState = PageStates.NoData;
                return;
            }
            Numerators = Settings.Subjects
                .GroupBy(item => item.Numerator, o => o)
                .Select(group => new NumeratorModel
                {
                    Name = group.Key,
                    Days = group.GroupBy(o => o.DayWeek, o => o)
                        .Select(i => new DayModel
                        {
                            Name = i.Key,
                            Subjects = i.Select(s => new SubjectModel
                            {
                                Auditory = s.AudNum.Replace(Environment.NewLine, ""),
                                Discipline = s.Discipline.Replace(Environment.NewLine, ""),
                                Lecturer =
                                    !string.IsNullOrWhiteSpace(s.Lecturer)
                                        ? s.Lecturer.Replace(Environment.NewLine, "")
                                        : null,
                                Time = s.Time.Replace(Environment.NewLine, "")
                            }).ToList()
                        }).ToList()
                }).ToList();
            Numerator = Numerators.FirstOrDefault();
            PageState = PageStates.Normal;
        }
    }
}

