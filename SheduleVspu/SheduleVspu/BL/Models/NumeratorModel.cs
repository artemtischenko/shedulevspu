using System.Collections.Generic;

namespace SheduleVspu.BL.Models
{
    public class NumeratorModel : BaseModel
    {
        public string Name { get; set; }
        public List<DayModel> Days { get; set; }
    }
}