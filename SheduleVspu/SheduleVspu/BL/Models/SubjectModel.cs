﻿namespace SheduleVspu.BL.Models
{
    public class SubjectModel
    {
        public string Time { get; set; }
        public string Discipline { get; set; }
        public string Lecturer { get; set; }
        public string Auditory { get; set; }

        public bool IsVisibleLecturer => !string.IsNullOrWhiteSpace(Lecturer);
    }
}
