using System.Collections.Generic;

namespace SheduleVspu.BL.Models
{
    public class DayModel : BaseModel
    {
        public string Name { get; set; }
        public List<SubjectModel> Subjects { get; set; }
    }
}