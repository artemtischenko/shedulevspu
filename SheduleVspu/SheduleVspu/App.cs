﻿using SheduleVspu.DAL.DataService;
using SheduleVspu.Helpers;
using SheduleVspu.UI.Pages;
using Xamarin.Forms;

namespace SheduleVspu
{
    public class App:Application
    {
        public App()
        {
            DataServices.Init();
            if (Settings.Subjects != null)
            {
                MainPage = new NavigationPage(new ShedulePage());
                return;
            }
            MainPage = new NavigationPage(new SettingsPage());
        }


    }
}
