﻿using System;
using SheduleVspu.BL.ViewModels;
using Xamarin.Forms;

namespace SheduleVspu
{
    public class BasePage:ContentPage
    {
        public BasePage()
        {
            var className = GetType().FullName.Replace("UI.Pages", "BL.ViewModels").Replace("Page", "ViewModel");
            var type = Type.GetType(className);
            BindingContext = Activator.CreateInstance(type);
            ViewModel.Navigation = Navigation;
        }
        public IViewModel ViewModel => BindingContext as IViewModel;

        protected override async void OnAppearing()
        {
            base.OnAppearing();
            await ViewModel.InitAsync();
        }

        protected override async void OnDisappearing()
        {
            base.OnDisappearing();
            await ViewModel.FinalizeAsync();
        }
    }
}